import { FastifyInstance } from 'fastify';
import Redis from '@fastify/redis';
import Swagger from '@fastify/swagger';
import SwaggerUI from '@fastify/swagger-ui';
import { TypeBoxTypeProvider } from '@fastify/type-provider-typebox';
import prismaPlugin from './plugins/prisma';
import sensible from './plugins/sensible';
import health from './routes/health';
import vehicle from './routes/vehicle';

/* eslint-disable-next-line */
export interface AppOptions {}

export async function app(fastify: FastifyInstance) {
  fastify.register(Redis, {
    url: process.env.REDIS_URL,
  });

  fastify.register(Swagger);
  fastify.register(SwaggerUI, {
    routePrefix: '/',
  });

  fastify.withTypeProvider<TypeBoxTypeProvider>();

  fastify.register(prismaPlugin);
  fastify.register(sensible);

  fastify.register(health, { prefix: '/health' });
  fastify.register(vehicle, { prefix: '/vehicle' });
}
