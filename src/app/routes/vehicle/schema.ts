import { type Static, Type } from '@sinclair/typebox';
import { FastifySchema } from 'fastify';

const Params = Type.Object({
  id: Type.Number(),
});
const QueryString = Type.Object({
  timestamp: Type.String({ format: 'date-time' }),
});

export type TParams = Static<typeof Params>;
export type TQueryString = Static<typeof QueryString>;

export interface IGetVehicle {
  Params: TParams;
  Querystring: TQueryString;
}

const BaseStateLog = Type.Object({
  vehicleId: Type.Number({ example: 1 }),
  state: Type.String({ example: 'quoted' }),
  timestamp: Type.String({
    format: 'date-time',
    example: '2022-09-10T10:23:54.000Z',
  }),
});

const Vehicles = Type.Object({
  id: Type.Number({ example: 1 }),
  make: Type.String({ example: 'BMW' }),
  model: Type.String({ example: 'X1' }),
  state: Type.String({ example: 'quoted' }),
});

const StateLog = Type.Intersect([
  BaseStateLog,
  Type.Object({
    vehicle: Vehicles,
  }),
]);

export const schema: FastifySchema = {
  params: Params,
  querystring: QueryString,
  response: { 200: StateLog },
};
