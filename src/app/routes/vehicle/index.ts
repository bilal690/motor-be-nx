import { type FastifyPluginAsync } from 'fastify';
import { IGetVehicle, schema } from './schema';

const vehicle: FastifyPluginAsync = async (fastify): Promise<void> => {
  fastify.get<IGetVehicle>('/:id', { schema }, async function (request, reply) {
    const { id } = request.params;
    const { timestamp } = request.query;

    // Attempt to retrieve the cached response from Redis
    const cacheKey = `${id}-${timestamp}`;
    const cachedResponse = (await fastify.redis.get(cacheKey)) ?? null;

    if (cachedResponse !== null) {
      // If the response is cached, return it directly
      const parsedResponse = JSON.parse(cachedResponse);
      void reply.header('x-cache', 'HIT');
      void reply.send(parsedResponse);
      return;
    }

    // find unique stateLog based on id and clostest timestamp
    const stateLog = await fastify.prisma.stateLogs.findFirst({
      where: {
        vehicleId: Number(id),
        timestamp: {
          lte: new Date(timestamp),
        },
      },
      include: {
        vehicle: true,
      },
    });

    if (stateLog) {
      await fastify.redis.set(cacheKey, JSON.stringify(stateLog), 'EX', 60);
      reply.send(stateLog);
    } else {
      reply.notFound('No entry found');
    }
  });
};

export default vehicle;
