import Fastify, { FastifyInstance } from 'fastify';
import { app } from '../../app';

describe('GET vehicle/:id', () => {
  let server: FastifyInstance;

  beforeEach(() => {
    server = Fastify();
    server.register(app);
  });

  it('should return 404 for a non-existant entry', async () => {
    const response = await server.inject({
      method: 'GET',
      url: '/vehicle/0',
      query: {
        timestamp: '2022-09-10T10:25:00Z',
      },
    });

    expect(response.statusCode).toEqual(404);
    expect(response.json().error).toEqual('Not Found');
  });

  it('should throw an error for an invalid id', async () => {
    const response = await server.inject({
      method: 'GET',
      url: '/vehicle/abc',
      query: {
        timestamp: '2022-09-10T10:25:00Z',
      },
    });

    expect(response.statusCode).toEqual(400);
    expect(response.json().message).toEqual('params/id must be number');
  });

  it('should throw an error for an invalid timestamp', async () => {
    const response = await server.inject({
      method: 'GET',
      url: '/vehicle/1',
      query: {
        timestamp: 'foobar',
      },
    });

    expect(response.json().message).toEqual(
      'querystring/timestamp must match format "date-time"'
    );
    expect(response.statusCode).toEqual(400);
  });
});
