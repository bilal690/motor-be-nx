import { FastifyPluginAsync } from 'fastify';

const health: FastifyPluginAsync = async (fastify): Promise<void> => {
  fastify.get(
    '/',
    {
      schema: {
        description: 'This is an endpoint for application health check',
        tags: ['health'],
        response: {
          200: {
            description: 'Success Response',
            type: 'object',
            properties: {
              message: { type: 'string' },
            },
          },
        },
      },
    },
    async function (_, reply) {
      reply.send({ message: 'Application is running.' });
    }
  );
};

export default health;
