import { PrismaClient } from '@prisma/client';

const prisma = new PrismaClient();

async function main() {
    await prisma.vehicles.create({
        data: {
            id: 1,
            make: 'BMW',
            model: 'X1',
            state: 'quoted',
        },
    });
    await prisma.vehicles.create({
        data: {
            id: 2,
            make: 'AUDI',
            model: 'A4',
            state: 'selling',
        },
    });
    await prisma.vehicles.create({
        data: {
            id: 3,
            make: 'VW',
            model: 'GOLF',
            state: 'sold',
        },
    });
    await prisma.stateLogs.createMany({
        data: [
            { vehicleId: 1, state: 'quoted', timestamp: new Date('2022-09-10T10:23:54Z') },
            { vehicleId: 2, state: 'quoted', timestamp: new Date('2022-09-10T14:59:01Z') },
            { vehicleId: 2, state: 'selling', timestamp: new Date('2022-09-11T17:03:17Z') },
            { vehicleId: 3, state: 'quoted', timestamp: new Date('2022-09-11T09:11:45Z') },
            { vehicleId: 3, state: 'selling', timestamp: new Date('2022-09-11T23:21:38Z') },
            { vehicleId: 3, state: 'sold', timestamp: new Date('2022-09-12T12:41:41Z') },
        ],
    });
}

main()
    .catch((e) => console.error(e))
    .finally(async () => {
        await prisma.$disconnect();
    });