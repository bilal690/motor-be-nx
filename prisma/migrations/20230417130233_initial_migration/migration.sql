-- CreateTable
CREATE TABLE "stateLogs" (
    "vehicleId" INTEGER NOT NULL,
    "state" TEXT NOT NULL,
    "timestamp" TIMESTAMPTZ(6) NOT NULL,

    CONSTRAINT "stateLogs_pkey" PRIMARY KEY ("vehicleId","timestamp")
);

-- CreateTable
CREATE TABLE "vehicles" (
    "id" INTEGER NOT NULL,
    "make" TEXT NOT NULL,
    "model" TEXT NOT NULL,
    "state" TEXT NOT NULL,

    CONSTRAINT "vehicles_pkey" PRIMARY KEY ("id")
);

-- AddForeignKey
ALTER TABLE "stateLogs" ADD CONSTRAINT "stateLogs_vehicleId_fkey" FOREIGN KEY ("vehicleId") REFERENCES "vehicles"("id") ON DELETE RESTRICT ON UPDATE CASCADE;
