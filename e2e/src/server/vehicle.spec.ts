import axios from 'axios';

describe('GET vehicle/:id', () => {
  it('should return a stateLog for a valid request', async () => {
    const stateLog = {
      vehicleId: 1,
      state: 'quoted',
      timestamp: '2022-09-10T10:23:54.000Z',
      vehicle: {
        id: 1,
        make: 'BMW',
        model: 'X1',
        state: 'quoted',
      },
    };

    const res = await axios.get(`/vehicle/1`, {
      params: {
        timestamp: '2022-09-10T10:30:00.000Z',
      },
    });

    expect(res.status).toEqual(200);
    expect(res.data).toEqual(stateLog);
  });
  it('should cache a response', async () => {
    const stateLog = {
      vehicleId: 2,
      state: 'quoted',
      timestamp: '2022-09-10T14:59:01.000Z',
      vehicle: {
        id: 2,
        make: 'AUDI',
        model: 'A4',
        state: 'selling',
      },
    };
    const res = await axios.get(`/vehicle/2`, {
      params: {
        timestamp: '2022-09-11T17:15:00.000Z',
      },
    });

    expect(res.headers['x-cache']).not.toEqual('HIT');
    expect(res.status).toEqual(200);
    expect(res.data).toEqual(stateLog);

    const res2 = await axios.get(`/vehicle/2`, {
      params: {
        timestamp: '2022-09-11T17:15:00.000Z',
      },
    });

    expect(res2.headers['x-cache']).toEqual('HIT');
    expect(res2.status).toEqual(200);
    expect(res2.data).toEqual(stateLog);
  });
});
